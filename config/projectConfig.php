<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/19
 * Time: 15:20
 */

return [

    // 前端Api签名验证 开发环境下关闭;
    'signVerify'            =>  true,

    // 是否使用ThinkPhP自带的异常类 在开发环境下开启;
    'ExceptionSwitch'       =>  true,



    // 后台静态文件目录
    '__ADMIN_STATIC__'       => '/admin',




];