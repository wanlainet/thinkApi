<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 中间件配置
// +----------------------------------------------------------------------
return [
    // 默认中间件命名空间
    'default_namespace' =>  'app\\http\\middleware\\',

    /** 验证sign */
    'SignVerify'        =>  app\http\middleware\SignVerify::class,

    /** 验证权限 */
    'Auth'              =>  app\http\middleware\Auth::class,

    /** 后台登录 */
    'IsLoginAdmin'      =>  app\http\middleware\IsLoginAdmin::class,

    /** 限流验证 */
    'ImposeRequest'     =>  app\http\middleware\ImposeRequest::class,
];
