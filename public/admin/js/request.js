function request($method,$url,$data,callback) {
    $.ajax({
        method:$method,
        url:$url,
        data:$data,
        dataType:'json',
        success:(res) => {
            callback(res);
        }
    });
}