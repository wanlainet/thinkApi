<?php


namespace app\lib\exception;


class ServiceErrorException extends BaseException
{
    public $code = 500;
    public $message = 'Service Error!';
    public $errorCode = 10001;
}