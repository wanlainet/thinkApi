<?php


namespace app\lib\exception;


use think\Exception;

class BaseException extends Exception
{

    /** @var int 参数错误状态码 */
    public $code = 400;
    public $message = '参数错误';
    /** @var int 自定义错误码 */
    public $errorCode = 1000;

    // TODO: __construct()不能传参
/*    public function __construct($params = [])
    {
        if (!is_array($params))
        {
            return ;
        }
        if (array_key_exists('code',$params))
        {
            $this->code = $params['code'];
        }
        if (array_key_exists('message',$params))
        {
            $this->code = $params['message'];
        }
        if (array_key_exists('errorCode',$params))
        {
            $this->code = $params['errorCode'];
        }
    }*/

}