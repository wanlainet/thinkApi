<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/19
 * Time: 15:11
 */

namespace app\api\controller;

use app\api\controller\BaseApiController as BAC;
use app\api\validate\DemoValidate;

class Index extends BAC
{
    /**
     * @throws \app\lib\exception\ParameterException
     */
    public function index()
    {
        (new DemoValidate())->HttpRequestCheck();
        echo 'this is new index';
    }
}