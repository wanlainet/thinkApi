<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/19
 * Time: 16:12
 */

namespace app\api\validate;


use app\lib\exception\ParameterException;
use think\facade\Request;
use think\Validate;

class BaseApiValidate extends Validate
{
    /**
     * @全局验证处理 前台传过来的值必须通过HttpRequestCheck方法进行验证
     * @throws ParameterException
     */
    public function HttpRequestCheck()
    {
        # Request实例化获取不到值
        $params = Request::instance()->param();
        //->batch()
        if (!$this->check($params))
        {
            throw new ParameterException($this->getError());
        }
        return;
    }

//    public function demo()
//    {
//        $userId = 1507474;
//        $team = db('team')->where(['pId'=>$userId])->select();
//        foreach ($team as $key => $value)
//        {
//            $value['']
//        }
//
//    }


}