<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/19
 * Time: 16:36
 */

namespace app\api\validate;


class DemoValidate extends BaseApiValidate
{
    protected $rule = [
        'id'    =>  'require|number',
        'name'  =>  'require'
    ];
}