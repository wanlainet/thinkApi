<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件
/* +----------------------------------------------------------------------
   | 所有模块共用   caviar<1159118839@qq.com>
   +----------------------------------------------------------------------*/

/**
 * { 请求成功返回函数 }
 * @param $message
 * @param null $data
 * @return \think\response\Json
 */
function http200($message = '成功!',$data = null)
{
    return json(['code'=>200,'msg'=>$message,'data'=>$data],200);
}


/**
 * { 权限不足返回函数 }
 * @param $message
 * @param null $data
 * @return \think\response\Json
 */
function http403($message = '您的权限不足!',$data = null)
{
    return json(['code'=>403,'msg'=>$message,'data'=>$data]);
}

/**
 * { 客户端请求的范围无效 }
 * @param $message
 * @param null $data
 * @return \think\response\Json
 */
function http416($message = '失败!',$data = null)
{
    return json(['code'=>416,'msg'=>$message,'data'=>$data]);
}

/**
 * { 服务器错误返回函数 }
 * @param $message
 * @param null $data
 * @return \think\response\Json
 */
function http500($message = '服务器内部错误,请联系后台管理人员!',$data = null)
{
    return json(['code'=>500,'msg'=>$message,'data'=>$data],500);
}

/* +----------------------------------------------------------------------
   | 常用常量   caviar<1159118839@qq.com>
   +----------------------------------------------------------------------*/

/** 数字一 */
const CONSTANT_ZERO = 0;

const CONSTANT_ONE = 1;

/** 数字二 */
const CONSTANT_TWO = 2;

/** 分页参数 */
const CONSTANT_PAGE = 10;






























