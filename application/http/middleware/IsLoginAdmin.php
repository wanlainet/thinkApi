<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/20
 * Time: 16:52
 */

namespace app\http\middleware;


class IsLoginAdmin
{
    /**
     * @Goal 中间件 处理登录
     * @param $request
     * @param \Closure $next
     * @return mixed|\think\response\Redirect
     */
    public function handle($request, \Closure $next)
    {
//        return empty(cookie('token')) ? redirect('admin/login/login') : $next($request);
        return $next($request);
    }


}