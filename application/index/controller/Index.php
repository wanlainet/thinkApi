<?php
namespace app\index\controller;



use app\common\service\LoadBalancing;
use app\lib\exception\ServiceErrorException;
use app\lib\redis\Lock;
use think\Controller;

class Index extends Controller
{
    // 中间件
    protected $middleware = [
        'ImposeRequest'
    ];

    /**
     * @throws ServiceErrorException
     */
    public function index()
    {
        $load = new LoadBalancing();

        echo $load->balanceAllot();

        $key = 'update:userId:1';

        $lock = new Lock($key);

        try{

            $lock->aLock();
            echo '资源没有被锁住!';

        } finally {

            $lock->unLock();

        }


    }

}
