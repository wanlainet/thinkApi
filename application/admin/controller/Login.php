<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/18
 * Time: 16:37
 */

namespace app\admin\controller;


use think\Request;

/**
 * Class Login
 * @package app\admin\controller
 * @ 登录注册
 */
class Login
{

    public function __construct()
    {

    }

    public function login(Request $request)
    {
        if ($request->isAjax()) {
            return $request->param();
        }
        return view();
    }

    public function logout()
    {
        return view();
    }






}