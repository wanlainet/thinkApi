<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/18
 * Time: 15:31
 */

namespace app\admin\controller;


use think\App;
use app\admin\controller\BaseAdminController as BAC;

class Index extends BAC
{

    public function __construct(App $app = null)
    {
        parent::__construct($app);
    }

    public function index()
    {
        return view();
    }

    public function welcome()
    {
        return view();
    }

}


