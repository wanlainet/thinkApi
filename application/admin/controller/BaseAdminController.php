<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/18
 * Time: 15:27
 */

namespace app\admin\controller;

use think\App;
use think\Controller;

class BaseAdminController extends Controller
{
    // 中间件
    protected $middleware = [
        'Auth',
        'IsLoginAdmin',
    ];

    /** @var mixed 前台参数 */
    static protected $BaseParams;

    /**
     * BaseAdminController constructor.
     * @param App|null $app
     */
    public function __construct(App $app = null)
    {
        parent::__construct($app);
        self::$BaseParams = $this->request->param();
    }





}







