<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/18
 * Time: 15:28
 */

namespace app\admin\model;


use think\Model;

class BaseAdminModel extends Model
{

    /** @var string 创建时间 */
    protected $createTime = 'createTime';

    /** @var string 修改时间 */
    protected $updateTime = 'updateTime';

    public function __construct($data = [])
    {
        parent::__construct($data);
    }

    # 获取某个Id的数据
    public function _findById($tableId)
    {
        return $this->where(['id'=>$tableId])->find();
    }

    # 获取Ids的数据
    public function _findByIds($tableIds)
    {
        return $this->where('id','in',$tableIds)->select();
    }

    # 分页获取数据
    public function _getTableListByPage($page = CONSTANT_PAGE)
    {
        return $this->paginate($page);
    }

    # 根据 where 查询记录
    public function _getTableListByWhere($where = null)
    {
        return $this->where($where)->select();
    }

    # 搜索
    public function _getTableListBySearch($name,$search)
    {
        return $this->where($name,'like','%'.$search.'%')->select();
    }













}









