<?php
/**
 * Created by PhpStorm.
 * User: caviar
 * Date: 2019/9/18
 * Time: 16:35
 */

namespace app\admin\service;


class Server
{
    public function __construct()
    {

    }

    /**
     * @return array 获取服务器参数
     */
    public function _getServiceParams()
    {
        return [
            's'             =>  php_uname(),
            'sysos'         =>  $_SERVER["SERVER_SOFTWARE"],
            'phpinfo'       =>  PHP_VERSION,
            'ip'            =>  $_SERVER['SERVER_ADDR'],
            'SERVER_NAME'   =>  $_SERVER['SERVER_NAME'],
        ];
    }





}